console.log('Hello World!')

let firstNumber = 12
let secondNumber = 5
let total = 0


//ARITHMETIC OPERATORS
// Addition Operator - responsible for adding two or more numbers
total = firstNumber + secondNumber
console.log('Result of addition operator is ' + total)

// Substraction Operator - responsible for the subtraction of two or more numbers
total = firstNumber - secondNumber
console.log('Result of subtraction operator is ' + total)

// Multiplication Operator - responsible for the multiplication of two or more numbers
total = firstNumber * secondNumber
console.log('Result of multiplication operator is ' + total)

// Subdivision Operator - responsible for the subdivision of two or more numbers
total = firstNumber / secondNumber
console.log('Result of subdivision operator is ' + total)

// Modulo Operator - responsible for getting the remainder of two or more numbers
total = firstNumber % secondNumber
console.log('Result of modulo operator is ' + total)


// ASSIGNMENTS OPERATORS
// Reassignment Operator - should be a single equal sign, signigies reassignment or new value into existing variable
total = 27
console.log('Result of reassignment operator is ' + total)

// Addition Assignment Operator - uses the current value of the variable and ADDS a number to itself. Afterwards, reassigns it as the new value

total += 3
// total = total + 3
console.log('Result of addition reassignment operator is ' + total)

// Subtraction Assignment Operator - uses the current value of the variable and SUBTRACTS a number to itself. Afterwards, reassigns it as the new value

total -= 5 
// total = total - 5
console.log('Result of subtraction reassignment operator is ' + total)

total *= 4
console.log('Result of multiplication reassignment operator is ' + total)



total /= 20
console.log('Result of division assignment operator is ' +total)

let mdasTotal = 0
let pemdasTotal = 0


// When doingg multiple operations, the program follows the MDAS rule (Multiplication then Division then Addition then Subtraction)
mdasTotal = 2 + 1 - 5 * 4 / 1
console.log('Result of 2 + 1 - 5 * 4 / 1 is ' + mdasTotal)

// Exponents are declared by using double asterisks (**) after the number 
pemdasTotal = 5**2 + (10-2) / 2 * 3
console.log('Result of 2 + 1 - 5 * 4 / 1 is ' + pemdasTotal)


// Pre - adds/minus 1 before the number
// Post - adds/minus 1 after the number
let incrementNumber = 1
let decrementNumber = 5
let resultOfPreIncrement = ++incrementNumber
let resultOfPreDecrement = --decrementNumber
let resultOfPostIncrement = incrementNumber++
let resultOfPostDecrement = decrementNumber--

console.log(resultOfPreIncrement)
console.log(resultOfPreDecrement)
console.log(resultOfPostIncrement)
console.log(resultOfPostDecrement)


//Coercion/Concatenation = adding a string value to a non-string value
let a = '10'
let b = 10

console.log(a+b)

//Non-Coercion - when you add 2 or more non string values
let d = 10
let f = 10

console.log(d+f)

// Typeof Keyword - returns the data type of a variable
let stringType = 'hello'
let numberType = 1
let booleanType = true
let arrayType = ['1', '2', '3']
let objectType = {
	objectKey: 'Object Value'
}


console.log(typeof stringType)
console.log(typeof numberType)
console.log(typeof booleanType)
console.log(typeof arrayType)
console.log(typeof objectType)

// Computer reads 'true' as 1
// Compute reads 'false' as 0
console.log(true + 1)
console.log(false + 1)

//COMPARISON OPERATORS
// Equality Operator - checks if both values are the same, returns true if it is
console.log(5 == 5)
console.log('hello' == 'hello')
console.log(2 == '2')

//Strict Equality Operator - checks if both values are data types are the same, returns true if it is 
console.log(2 === '2')
console.log(true === '1')

//Inequality Operator - checks if both values are NOT equal, returns true if it is
//if not equal = true
//if equal = false 
console.log(1 != 1)
console.log('hello' != 'hello')
console.log(2 != '2')


//Strict Inequality Operator - checks if both values AND data type are not equal, returns true if they aren't
console.log(1 !== '1')


//RELATIONAL OPERATORS
let firstVariable = 5
let secondVariable = 5

// Greater than / GT Operator checks if first value is greater than the second
console.log(firstVariable > secondVariable)
// Less than / LT Operator checks if first value is lesser than the second
console.log(firstVariable < secondVariable)

//Greater than or equal / GTE Operator checks if first value is either greater than OR equal to the second
console.log(firstVariable >= secondVariable)
//Less than or equal / LTE Operator checks if first value is either less than OR equal to the second
console.log(firstVariable <= secondVariable)



// LOGICAL OPERATORS
let isLegalAge = true
let isRegistered = false


//The && and || Operators in JavaScript. If applied to boolean values, the && operator only returns true when both of its operands are true (and false in all other cases), while the || operator only returns false when both of its operands are false (and true in all other cases).
// AND Operator - returns true if both statements are true. Returns false if one of them is not true
console.log(isLegalAge && isRegistered)

//sa OR, if one value is true, returns true 
// OR Operator - returns true if only one of the statements are true. Returns false if NONE of the statements are true
console.log(isLegalAge || isRegistered)

// NOT Operator Reverses the boolean value ( from true to false or vice versa)
console.log(!isLegalAge)

// true + not true = true
console.log(isLegalAge && !isRegistered)

//not true + false = false
console.log(!isLegalAge || isRegistered)


// Truthy and Falsey values
//Everything that is either empty, zero, or null will equate to false, and everything that has some sort of value will equate to true
console.log('0' == true)

console.log([] == false)

console.log('' == false)

//console.log(null == false) - this is false because null is undefined

console.log(0 == false)

//let userLoggedIn = 'Earl Diaz'

//if(userLoggedIn)
//process content ng website or software
